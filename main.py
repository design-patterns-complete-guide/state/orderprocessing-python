# Define the State interface
class OrderState:
    def process_order(self, order):
        pass

    def ship_order(self, order):
        pass

    def deliver_order(self, order):
        pass

# Concrete state classes
class PendingState(OrderState):
    def process_order(self, order):
        print("Processing the order.")
        order.set_state(ProcessingState())

    def ship_order(self, order):
        print("Cannot ship a pending order.")

    def deliver_order(self, order):
        print("Cannot deliver a pending order.")

class ProcessingState(OrderState):
    def process_order(self, order):
        print("Order is already being processed.")

    def ship_order(self, order):
        print("Shipping the order.")
        order.set_state(ShippedState())

    def deliver_order(self, order):
        print("Cannot deliver an order that hasn't been shipped.")

class ShippedState(OrderState):
    def process_order(self, order):
        print("Cannot process a shipped order.")

    def ship_order(self, order):
        print("Order is already shipped.")

    def deliver_order(self, order):
        print("Delivering the order.")
        order.set_state(DeliveredState())

class DeliveredState(OrderState):
    def process_order(self, order):
        print("Cannot process a delivered order.")

    def ship_order(self, order):
        print("Cannot ship a delivered order.")

    def deliver_order(self, order):
        print("Order is already delivered.")

# Context class
class Order:
    def __init__(self):
        self.state = PendingState()  # Initial state is "Pending"

    def set_state(self, state):
        self.state = state

    def process(self):
        self.state.process_order(self)

    def ship(self):
        self.state.ship_order(self)

    def deliver(self):
        self.state.deliver_order(self)

# Example usage
order = Order()
order.process()  # Output: Processing the order.
order.ship()     # Output: Shipping the order.
order.deliver()  # Output: Delivering the order.
order.ship()     # Output: Cannot ship a delivered order.
